# Copyright (C) 2010 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(my-dir)

CAMERA_DEFAULT := $(TARGET_OUT_SHARED_LIBRARIES)/hw/camera.armadillo800eva$(TARGET_SHLIB_SUFFIX)

include $(CLEAR_VARS)

LOCAL_MODULE := camera.armadillo800eva
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := FAKE
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE): CAMERA_LIB := camera.v4l$(TARGET_SHLIB_SUFFIX)
$(LOCAL_BUILT_MODULE):
	$(hide) echo "Linking $(CAMERA_LIB) <- $(CAMERA_DEFAULT)"
	$(hide) mkdir -p $(TARGET_OUT_SHARED_LIBRARIES)/hw
	$(hide) ln -sf $(CAMERA_LIB) $(CAMERA_DEFAULT)

include $(call all-makefiles-under,$(LOCAL_PATH))
